from random import *

def lettre_dans_mot(lettre, mot_a_trouver):
    positions = [] 

    position_actuelle = 0 
    for lettre_mot in mot_a_trouver:
        if lettre_mot == lettre: #si le mot est le meme que la lettre entrez par le joueur
            positions.append(position_actuelle) #ajoute a la position actuelle la lettre que le joueur vient de trouver
    position_actuelle += 1 # parcours le mot lettre par lettre

    return positions

def affichage_lettres_trouvées(position, mot_a_touver):
    mot_a_afficher = "" #mot vide dans lequelle il n'y a rien, il va etre construit au fur et a mesure

    position_actuelle = 0
    for lettre_mot in mot_a_trouver:
        if position_actuelle in positions:
            mot_a_afficher += lettre_mot #on ajoute au mot a afficher la lettre du mot

        else:
            mot_a_afficher += '-' #sinon on ajoute un tiret
        positions_actuelle += 1

        return mot_a_afficher

def choisir_mot(nom_fichier):
    liste_de_mot = [] 

    fichier = open(nom_fichier,"r") #ouvre le fichier
    for ligne in fichier:
        ligne = ligne.strip("\n") #strip sert à supprimer les caractères de gauche et de droite de l'argument. Donc là strip supprime apostrophes 
        liste_de_mot.append(ligne) #on ajoute la ligne qu'on a fait jsute au dessus à la liste de mot

    shuffle(liste_de_mot) # shuffle est une fonction de random qui sert va mélanger tout la liste et l'on aura plus qu'a prendre le premier terme de cette liste
    mot_a_trouver=liste_de_mot[0] #prend le premier mot de la liste de mot

    return mot_a_trouver

mot_a_trouver=choisir_mot("mot.txt") #les mots viennent du fichier mot.txt
position_tout = [] #position_tout stocke la position des lettres trouvés
print(mot_a_trouver)
essais=0
lettres_trouvees=[] #lettres_trouvées stocke dans une liste toutes les lettres trouvés

PENDAISON = [''' 

  +---+
  |   |
      |
      |
      |
      |
=========''','''

  +---+
  |   |
  0   |
      |
      |
      |
=========''','''

  +---+
  |   |
  0   |
  |   |
      |
      |
=========''','''

  +---+
  |   |
  0   |
 /|   |
      |
      |
=========''','''

  +---+
  |   |
  0   |
 /|\  |
      |
      |
=========''','''

  +---+
  |   |
  0   |
 /|\  |
 /    |
      |
=========''','''

  +---+
  |   |
  0   |
 /|\  |
 / \  |
      |
=========''']

while essais<6: #le jour a 6 essais (si il ne se trompe pas, il ne predra pas d'essais)
    lettre_joueur=input("Entrez une lettre : ")
    while lettre_joueur in lettres_trouvées: #stocke les lettres qu'on a déjà trouvées
        lettre_joueur=input("Vous avez déjà entrer cette lettre. Entrez une autre lettre : ")
    lettres_trouvees.append(lettre_joueur) # ajoute les lettres_trouvees a la liste Lettres_trouvées
    positions_actuelle=lettre_dans_mot(lettre_joueur,mot_a_trouver) #
    if positions_actuelle == []:  #le joueur a le droit à 6 erreur donc on enlève un essais seulemement quand la liste est vide
        essais+=1
    position_tout+=positions_actuelle #mise a jour de la position des lettres si on a trouve une nouvelle lettre
    mot_a_afficher=affichage_lettres_trouvées(position_tout,mot_a_trouver) #les lettres que l'on a trouve sont affiché
    print(mot_a_afficher)
    print(PENDAISON[essais]) #le nombre d'essais est representer par la pendaison
    if mot_a_trouver == mot_a_trouver: #si on trouve le mot alors on gagne
        print("vous avez gagné :)")
        exit() #quitte la boucle pour ne pas qu'elle continue indéfiniment